require 'sinatra/base'
require 'haml'
require 'compass'

class CCA < Sinatra::Base
  class_eval open('./lib/configure.rb').read
  class_eval open('./lib/helpers.rb').read
  
  get '/*.css' do |name|
    scss :"stylesheets/#{name}"
  end

  #FIXME: Config compass sprites properly
  get '/*.png' do |name|
    redirect to("/images/#{name}.png")
  end
  get '/' do
    redirect to('/services.html')
  end
  
  get '/services.html' do
    current = :services
    sub = :'cloud computing'
    haml :vm_room, :locals => {
      :vm => vm,
      :current => titlefy(current),
      :sub => titlefy(sub),
      :sub_nav => nav[current],
      :nav => nav
    }
  end
  get '/overview.html' do
    current = :overview
    sub = :dashboard
    haml :dashboard, :locals => {
      :current => titlefy(current),
      :sub => titlefy(sub),
      :sub_nav => nav[current],
      :nav => nav
    }
  end
  get '/monitoring.html' do
    current = :monitoring
    haml current, :locals => {
      :current => titlefy(current),
      :sub_nav => {},
      :nav => nav
    }
	
  end
  get '/billing.html' do
	current = :billing
    haml current, :locals => {
      :current => titlefy(current),
      :sub_nav => {},
      :nav => nav
    }

  end
  get '/documents.html' do
	current = :documents
    haml current, :locals => {
      :current => titlefy(current),
      :sub_nav => {},
      :nav => nav
    }
  end
  get '/orders.html' do
	current = :orders
    haml current, :locals => {
      :current => titlefy(current),
      :sub_nav => {},
      :nav => nav
    }
  end
  get '/service-desk.html' do
	current = :'service-desk'
    haml current, :locals => {
      :current => titlefy(current),
      :sub_nav => {},
      :nav => nav
    }
  end
  get '/colors.html' do
    c = File.read('./views/stylesheets/colors.scss').split.map{|x| x.split(':')}.flatten
    h = Hash[*c]
    content = ""
    h.each do |k, v|
      content << "<tr>"
      content << "<td>#{k}</td>"
      content << "<td style='background:#{v}'><td>"
      content << "</tr>"
    end
    table = "<table><tr><th>name</th><th>color</th></tr>#{content}</table>"
  end
  get '/test.html' do
    current = :services
    haml :'test', :locals => {
      :current => titlefy(current),
      :sub_nav => nav[current],
      :nav => nav
    }
  end
end
