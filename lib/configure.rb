configure do
  Compass.configuration do |config|
    config.project_path = File.dirname(__FILE__)
    config.sass_dir = 'views/stylesheets'
    config.images_path = 'public/images'
  end

  use Rack::Session::Pool, :expire_after => 2592000

  set :haml, { :format => :html5 }
  sass_options = Compass.sass_engine_options
  sass_options[:line_comments] = false;
  set :scss, sass_options
  set :logging, :true
  set :public, File.dirname(__FILE__) + '/public'
end
