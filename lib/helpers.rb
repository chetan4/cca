helpers do
  def vm
    count = 0
    type = ['windows', 'linux', 'redhat', 'ubuntu', 'centos']
    states = {
      'start' => 'started',
      'shutdown' => 'shutdown',
      'pause' => 'paused',
      'reboot' => 'rebooting',
      'launch' => 'launched',
      'stop' => 'stopped',
      'restart' => 'restarting',
      'details' => 'details'
    }
    name = [
            'Penguin', 'Owl', 'Finch', 'Dove', 'Cardinal', 'Eagle', 'Kiwi',
            'Wren', 'Starling', 'Heron', 'Peacock', 'Rooster', 'Chick',
            'Ostrich', 'Emu', 'Condor', 'Falcon', 'Hummingbird', 'Sparrow',
            'Kingfisher', 'Woodpecker', 'Roadrunner'
           ]
    length = name.length
    actions = states.keys
    states = states.values - ['details']
    states + ['launched', 'launched']
    lambda {
      {
        :id => count += 1,
        :type =>  type.sample,
        :state => states.sample,
        :name => name.pop,
        :user => "evl#{rand(10000000)}",
        :ip => (1..4).map{rand 255}.join('.'),
        :actions => actions
      }
    }
  end

  def slugfy sym
    classfy(sym).gsub('_', '-')
  end
  def classfy sym
    sym.to_s.gsub(/\b\w/){$&.downcase}.gsub(' ', '_')
  end
  def titlefy sym
    sym.to_s.gsub(/\b\w/){$&.upcase}
  end
  def class_string(cls, current)
    "#{cls} #{'selected' if cls == current}"
  end
  def nav
    @@nav ||= {
      :overview => {
        :dashboard => {:icon => '/images/dashboard.png'},
        :favorites => {:icon => '/images/fav_grey.png'}
      },
      :services => {
        :'all services' => {},
        :'cloud computing' => {:icon => '/images/cloud_color.png'},
        :'dedicated servers' => {:icon => '/images/dedicated_servers.png'},
        :'managed security' => {:icon => '/images/shield_grey.png'},
        :'data backup' => {:icon => '/images/folder_grey.png'},
        :'load balancers' => {:icon => '/images/load_balancer_grey.png'},
        :'dns' => {:icon => '/images/globe_grey.png'}
      },
      :monitoring => {},
      :billing => {},
      :orders => {},
      :documents => {},
      :'service desk' => {}
    }
  end
  def contacts
    {
      'Technical Account Manager' => {
        :name => 'Craig Shottenheimer',
        :email => 'c.schnitzel@ntta.com',
        :phone => '1-111-222-3333'
      },
      'Account Manager' => {
        :name => 'Emily Jablonski',
        :email => 'e.jablonski@ntta.com',
        :phone => '1-123-456-8888'
      },
      'Technical Support' => {
        :email => 'support@ntta.com',
        :phone => '9-992-838-8732'
      },
      'Billing Support' => {
        :email => 'billing@ntta.com',
        :phone => '3-838-383-8747'
      }
    }
  end
  def menu_icon(menu_item)
    @@icons ||= {
      'Cloud Computing' => '/images/cloud_color.png',
      'Dedicated Servers' => '/images/dedicated_servers.png',
      'Managed Security' => '/images/shield_grey.png',
      'Data Backup' => '/images/folder_grey.png',
      'Load Balancers' => '/images/load_balancer_grey.png',
      'DNS' => '/images/globe_grey.png'
    }
    @@icons[menu_item] || nil
  end

  def random_bits
    ['32-bit','64-bit','128-bit'].sample
  end

  def os_version(type)
    @@os_versions ||= {'ubuntu' => ['12.04 LTS','6.10','7.10', '8.10', '9.10', '10.04 LTS','10.10','11.04','11.10'],
      'linux' => ['3.0.3','2.6.39.4 ','2.4.37.11', '2.2.26', '2.0.40', '1.2.13','1.0.9'],
      'windows' => ['7', 'Server 2008', 'Server 2008', 'Vista', 'XP Home', 'XP Std'],
      'centos' => ['2','3.1','3.3','3.4','3.5','3.6','3.7','3.8','3.9','4''4.1','4.2','4.3','4.4','4.5','4.6','4.7','4.8', '4.9', '5','5.1','5.2','5.3','5.4','5.5','5.6','5.7','6 ','6.1'],
      'redhat' => ['6.2','7.2','6']
    }
    @@os_versions[type].sample
  rescue
    ''
  end
end
