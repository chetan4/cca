#! /usr/bin/ruby
require 'fileutils'
file_group = Dir.glob('*.png').sort.each_slice(2).to_a
file_group.each do |group|
  color, bw = group
  new_bw = bw.gsub '_bw', '_active'
  FileUtils.mv bw, new_bw
end
