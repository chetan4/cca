#! /usr/bin/ruby

task :swap_active_hover do
  require 'fileutils'
  file_group = Dir.glob('*.png').sort.each_slice(3).to_a
  file_group.each do |group|
    grey, light, dark = group
    dark_bak = dark+'.bak'
    FileUtils.mv dark, dark_bak
    FileUtils.mv light, dark
    FileUtils.mv dark_bak, light
  end
end

task :rename, [:name, :new_name] do |t, args|
  
end
