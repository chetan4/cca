//services dash
$(function() {
  var $vm_container = $(".vms");
  var $vm_actions = $('.vm-actions a');
  var $vm_links = $('.vm-links a');
  var $vm_lis = $('.vms > li');
  var filter_selectors = {
    'All': '*',
    'Windows':  '.windows',
    'Red Hat': '.redhat',
    'Linux': '.linux',
    'Ubuntu': '.ubuntu',
    'CentOS': '.centos',
    'Good': '.launched',
    'Bad': '.started',
    'Marginal': '.paused',
    'Off': '.shutdown'
  };
  var $vm_view_classes = $(['tiny', 'normal', 'bar']);
  var layout_map_classes = {
    'cards': 'normal',
    'lists': 'bar',
    'icons': 'tiny'
  };
  var state_map_classes = {
    'launch': 'launched',
    'pause': 'paused',
    'reboot': 'rebooting',
    'restart': 'restarting',
    'shutdown': 'shutdown',
    'start': 'started',
    'stop': 'stopped'
  }
  var $heading = $ (
    '<h1 class=vm-list-heading>'+
      '<span class=nick>Nick name</span>'+
      '<span class=id>Service ID</span>'+
      '<span class=cpu>CPU</span>'+
      '<span class=memory>Memory</span>'+
      '<span class=disk>Disk</span>'+
      '</h1>'
  );
  $ ('.vms').before ($heading.hide ());
  function reLayout() {
    $vm_container.isotope('reLayout');
  }

  $vm_container.isotope({ 'masonry': {'columnWidth': 1}, 'animationEngine': 'jquery' });

  $vm_actions.click(function() {
    var $this = $(this);
    var selector = filter_selectors[$this.text().trim()];
    $vm_container.isotope({
      'filter': selector
    });
    return false;
  });

  $vm_links.click(function(e) {
    var $link = $(this);
    var cl = $link.attr('class').split (' ') [0];
    var target_cl = layout_map_classes[cl];
    target_cl === 'bar' ? $heading.show () : $heading.hide ();
    $vm_lis.each(function() {
      var $vm = $(this);
      $vm.trigger('select', [target_cl]);
    });
    reLayout ();
    return false;
  });

  var dialog_html =
    "<div class=vitals>" +
    "<ul>" +
    "<li><a href=#cpu  class=green>CPU</a></li>" +
    "<li><a href=#memory class=green>Memory</a></li>" +
    "<li><a href=#disk class=green>Disk</a></li>" + 
    "<li><a href=#ping class=green>Ping</a></li>" + 
    "<li><a href=#ping class=green>TCP Port 22</a></li>" + 
    "<li><a href=#ping class=green>VM Disk 1/0</a></li>" + 
    "<li><a href=#ping class=green>VM Network 1/0</a></li>" + 
    "<li><a href=#ping class=green>VM Performance</a></li>" + 
    "</ul>" +
    "<div id=cpu></div><div id=memory></div><div id=disk></div><div id=ping></div>" +
    "</div>";
  var $dialog = $ (dialog_html).dialog ({
    autoOpen: false, title: 'Vital stats',
    width: 800, height: 450,
    dialogClass: 'vital-stats', modal: true
  }).tabs ();

  var state_classes = _.values (state_map_classes);
  var view_names = ['card', 'icon', 'list'];
  function change_tool_tip ($element, tool_tip) {
    var old_title = $element.attr ('title');
    $element.attr ('title', tool_tip);
    init_tip ();
    return old_title;
  }

  $vm_lis.each(function() {
    var $vm = $(this), view_class_index = 0, state;
    $cycle = $vm.find('.cycle');
    $cycle.attr ('title', 'Click to select card view');

    //get initial state of $vm and remove state class
    _.each (state_classes, function (cls, index) {
      if ($vm.hasClass (cls)) { state = cls; $vm.removeClass (cls); }
    });

    //init states to what we care about
    if (state === 'rebooting') state = 'launched';
    if (state === 'restarting') state = 'launched';
    if (state === 'stopped') state = 'launched';

    //reflect in DOM
    $vm.addClass (state);


    function vm_view_classfy() {
      _(_.values (layout_map_classes)).each (function (cl) { $vm.removeClass (cl); });
      var view_class = $vm_view_classes[view_class_index]
      $vm.addClass(view_class);
      $vm.trigger (view_class + '_view');
    }
    function vm_change_state (action) {
      var previous_state = state;

      var is_up = state === 'started' || state === 'launched';
      if (state === 'started' && action === 'launch') state = 'launched';
      if (is_up) {
        if (action === 'pause') state = 'paused';
        if (action === 'shutdown') state = 'shutdown';
        if (action === 'reboot') state = 'started';
        if (state === 'launched' && action === 'stop') state = 'started';
      } else if (
        state === 'paused' && action === 'pause' ||
          state === 'shutdown' && action === 'start'
      ) state = 'started';

      $vm.removeClass (previous_state);
      $vm.addClass (state);
    }
    function bindActionButton(element){
      var $button = $(element);
      var action = $button.attr('class').split(' ')[0];
      vm_change_state (action);
      return false;
    }
    $vm.bind('select', function(e, cl) {
      view_class_index = _.indexOf($vm_view_classes, cl);
      vm_view_classfy();
    });
    $cycle.click(function() {
      $cycle = $ (this);
      if ($vm_view_classes [view_class_index] === 'tiny') {
        $vm.trigger('select', ['normal']);
        change_tool_tip ($cycle, 'Click to select icon view');
      } else {
        $vm.trigger('select', ['tiny']);
        change_tool_tip ($cycle, 'Click to select card view');
      }
      reLayout ();
    });
    $vm.find('.actions button').click (function () {
      bindActionButton(this);
    });
    $vm.find('.toolbar [title="actions"] button').click (function () {
      bindActionButton(this);
    });
    $vm.find ('.toggle').click (function () {
      $vm.trigger ('select', ['tiny']);
      reLayout ();
    });
    
    var $my_spark_lines = $vm.find ('.graph');
    $my_spark_lines.each (function (i) {
      var $graph = $(this);
      var type = $graph.attr('title');
      var our_types = $my_spark_lines.map ( function () {return $ (this).attr ('title');} );
      var machine_name = $vm.find ('.meta h1').text ().trim ();
      var user_name = $vm.find ('.meta h2').text ().trim ();
      var machine_ip = $vm.find ('.meta .ip').text ().trim ();
      var get_data = function () {
        return  _.map(_.range(5), function() { return Math.ceil(Math.random()*200) + 50; });
      };
      var data = get_data ();
      var chart_options = spark_line_options;
      chart_options.chart.renderTo = $graph [0];
      chart_options.series [0].data = data;
      var chart;
      function graph_click_handler () {
        var title_string = 'Vital signs : ';
        var tab_index = _.indexOf (our_types, type);
        title_string += machine_name + ' <em>' + user_name + '</em> ' + machine_ip;
        $dialog.dialog ('option', 'title', title_string);
        $dialog.tabs ('select', tab_index);
        $dialog.dialog ('open');
        $my_spark_lines.trigger ('populate_tab');
      }
      function spark_line_handler() {
        if (!chart) {
          if (type === 'ping') {
            return;
          }
          chart_options.chart.renderTo = $graph [0];
          chart_options.series [0].data = data;
          chart = new Highcharts.Chart (chart_options);
          $graph.find ('svg').andSelf ().click (graph_click_handler);
        } else {
          var graph_width = $graph.width (), graph_height = $graph.height ();
          chart.setSize (graph_width, graph_height);
        }
      }
      $vm.bind ('normal_view', spark_line_handler);
      $vm.bind ('bar_view', spark_line_handler);
      $graph.bind ('populate_tab', function () {
        var $tab = $dialog.find ('#'+type);
        var ids = {
          'memory': 'graph_49283_summary_0',
          'disk': 'graph_18383_disk_0',
          'cpu': 'graph_18385_cpu_0',
          'ping': 'graph_18384_packets_1'
        };
        var id = ids [type];
        if (id) {
          var detailed_chart_options = graph[id];
          _.extend (detailed_chart_options.chart, {
            renderTo: $tab [0],
            width: 740, height: 300
          });
          detailed_chart_options.series = details [id];
        }
        $tab.html ("");
        new Highcharts.Chart (detailed_chart_options);
      });
      $graph.find ('svg').andSelf ().click (graph_click_handler);
    });
  });
  
  $('.vm-links a').click(function() {
    $('.vm-links a').removeClass('selected');
    $(this).addClass('selected');
  });

  $('.vm-actions a').click(function() {
    $('.vm-actions a').removeClass('selected');
    $(this).addClass('selected');
  });

  $('.fav').click(function(){
    if ($(this).hasClass('favorited')){
      $(this).removeClass('favorited');
      $(this).attr('title', 'Mark as favorite');
      $(this).tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'left',delay: 10});
    }else{
      $(this).addClass('favorited');
      $(this).attr('title', 'Unmark as favorite');
      $(this).tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'left',delay: 10});
    }
  });

  function init_tip () {
    $(".tipTop").tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'top',delay: 10});
    $(".tipBottom").tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'bottom',delay: 10});
    $(".tipLeft").tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'left',delay: 10});
    $(".tipRight").tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'right',delay: 10});
    $(".tipRightHTML").tipTip({maxWidth: "auto", edgeOffset: 0,defaultPosition: 'right',delay: 10, content: false});
  }
  init_tip ();

});

//overview dash
$ (function () {

  $overview_dash_container = $ ('section#dashboard');
  $overview_dash_container.isotope({ 'masonry': {'columnWidth': 1} });

  (function usage () {
    var data = {
      'CPU': 56,
      'MEMORY': 32,
      'DISK': 92,
      'BACKUP': 104
    };
    var $target = $ ('.usage .body');
    var chart_options = {
      chart: {
        renderTo: $target [0],
        defaultSeriesType: 'column',
        margin: [0,0,30,0]
      },
      title: {text: ''},
      credits: {enabled: false},
      xAxis: {
        categories: _.keys (data)
      },
      legend: { enabled: false },
      plotOptions: {
        series: {
          animation: {
            duration: 2000
          }
        }
      },
      series: [{
        data: _.values (data)
      }]
    };

    new Highcharts.Chart (chart_options);
  } ());

});
